package be.cities.service;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import be.cities.model.Station;
import be.cozmos.kdg.gradleapplication.R;

/**
 * @author Jo Somers
 */
public class CityService {

    private static final String BASE_URL = "http://api.citybik.es/";

    public interface NetworkListener {
        void failure();

        void success(List<Station> stations);
    }

    public static void get(
            final Context context,
            final NetworkListener networkListener) {

        new AsyncTask<Void, Void, List<Station>>() {

            @Override
            protected List<Station> doInBackground(Void... params) {
                HttpURLConnection connection = null;
                try {
                    URL url = new URL(BASE_URL + context.getString(R.string.city_id) + ".json");

                    connection = (HttpURLConnection) url.openConnection();
                    connection.setConnectTimeout(1000);
                    connection.setReadTimeout(5000);

                    return new Gson().fromJson(IOUtils.toString(connection.getInputStream()),
                            new TypeToken<List<Station>>() {
                            }.getType());
                } catch (IOException e) {
                    return null;
                } finally {
                    if (connection != null) {
                        connection.disconnect();
                    }
                }
            }

            @Override
            protected void onPostExecute(List<Station> station) {
                super.onPostExecute(station);
                if (station != null) {
                    networkListener.success(station);
                } else {
                    networkListener.failure();
                }
            }
        }.execute();
    }

}
