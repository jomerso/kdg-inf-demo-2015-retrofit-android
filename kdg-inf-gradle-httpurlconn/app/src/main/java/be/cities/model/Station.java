package be.cities.model;

import com.google.gson.annotations.SerializedName;

/**
 * @author Jo Somers
 */
public class Station {

    @SerializedName("name")
    private String name;

    public Station() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
