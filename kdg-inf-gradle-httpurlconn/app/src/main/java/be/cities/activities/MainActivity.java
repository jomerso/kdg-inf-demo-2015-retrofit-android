package be.cities.activities;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import be.cities.model.Station;
import be.cities.service.CityService;
import be.cozmos.kdg.gradleapplication.R;


public class MainActivity extends Activity {

    private TextView cityInfoTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cityInfoTextView = (TextView) findViewById(R.id.cityInfo);
        cityInfoTextView.setText("...");
    }

    @Override
    protected void onResume() {
        super.onResume();

        final CityService.NetworkListener networkListener = new CityService.NetworkListener() {
            @Override
            public void failure() {
                Toast.makeText(getApplicationContext(), "Oops, something went wrong.",
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void success(List<Station> stations) {
                cityInfoTextView.setText("### " + stations.size() + " ###");
            }
        };

        CityService.get(this, networkListener);
    }

}
