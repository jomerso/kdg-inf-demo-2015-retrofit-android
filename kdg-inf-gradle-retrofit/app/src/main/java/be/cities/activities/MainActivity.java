package be.cities.activities;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import be.cities.R;
import be.cities.application.CityApplication;
import be.cities.model.Station;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MainActivity extends Activity {

    private TextView cityInfoTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cityInfoTextView = (TextView) findViewById(R.id.cityInfo);
        cityInfoTextView.setText("...");
    }

    @Override
    protected void onResume() {
        super.onResume();

        final Callback<List<Station>> callback = new Callback<List<Station>>() {
            @Override
            public void success(List<Station> stations, Response response) {
                cityInfoTextView.setText("### " + stations.size() + " ###");
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getApplicationContext(), "Oops, something went wrong.",
                        Toast.LENGTH_SHORT).show();
            }
        };

        CityApplication.getCityRetrofitService()
                .getStations(getString(R.string.city_id), callback);
    }

}
