package be.cities.application;

import android.app.Application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Random;

import be.cities.service.CityRetrofitService;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * @author Jo Somers
 */
public class CityApplication extends Application {

    private static CityRetrofitService cityRetrofitService;

    @Override
    public void onCreate() {
        super.onCreate();

        cityRetrofitService = createCityService();
    }

    public static CityRetrofitService getCityRetrofitService() {
        return cityRetrofitService;
    }

    private CityRetrofitService createCityService() {
        final Gson gson = new GsonBuilder().create();

        final RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                if (isUserLoggedIn()) {
                    request.addHeader("Authorization", getToken());
                }
            }
        };

        return new RestAdapter.Builder()
                .setConverter(new GsonConverter(gson))
                .setEndpoint("http://api.citybik.es")
                .setRequestInterceptor(requestInterceptor)
                .build()
                .create(CityRetrofitService.class);
    }

    private boolean isUserLoggedIn() {
        return new Random().nextBoolean(); // stub
    }

    private String getToken() {
        return "1234"; // stub
    }
}
