package be.cities.service;

import java.util.List;

import be.cities.model.Station;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Path;

/**
 * @author Jo Somers
 */
public interface CityRetrofitService {

    @GET("/{city_id}.json")
    void getStations(@Path("city_id") String cityId,
                     Callback<List<Station>> callback);

}
